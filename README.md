<div align="center">
    <img src="doc/LogoMakr_01dpIG.png" width="400px">
</div>

# Preview

![](doc/1.jpg)

# Usage

- Install [Stylish](https://userstyles.org/) plug-in in the browser.
- Go to [Leetcode Dark Theme](https://userstyles.org/styles/142782/leetcode-dark-theme), and click installs button. 
- Change Code Editor Settings in Leetcode.

<div align="center">
    <img src="doc/2.jpg" width="600px">
</div>



